import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringArithmeticTest {

    private StringArithmetic stringArithmetic;
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        stringArithmetic = new StringArithmetic();
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
        stringArithmetic = null;
    }

    @Test
    void testAdditionOfStrings(){
        assertEquals("4", stringArithmetic.operate("2","2","+"));
        assertEquals("14", stringArithmetic.operate("2","12","+"));
        assertEquals("2", stringArithmetic.operate("-3","5","+"));
    }

    @Test
    void testSubtractionOfStrings(){
        assertEquals("0", stringArithmetic.operate("2","2","-"));
        assertEquals("-5", stringArithmetic.operate("0","5","-"));
    }

    @Test
    void testMultiplicationOfNegativeStrings(){
        assertEquals("4", stringArithmetic.operate("-2","-2","*"));
        assertEquals("0", stringArithmetic.operate("0","-5","*"));
    }

    @Test
    void testMultiplicationOfStrings(){
        assertEquals("4", stringArithmetic.operate("2","2","*"));
        assertEquals("6", stringArithmetic.operate("2","3","*"));
        assertEquals("-15", stringArithmetic.operate("-3","5","*"));
    }

    @Test
    void testThatNullOperandsThrowException(){
        assertThrows(IllegalArgumentException.class, ()-> stringArithmetic.operate(null, null, "+"));
        assertThrows(IllegalArgumentException.class, ()-> stringArithmetic.operate("", "", "+"));
        assertThrows(IllegalArgumentException.class, ()-> stringArithmetic.operate(null, "5", "+"));
        assertThrows(IllegalArgumentException.class, ()-> stringArithmetic.operate("3", "5", ""));
        assertThrows(IllegalArgumentException.class, ()-> stringArithmetic.operate("5", "5", "@"));
    }
}