import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TennisScoreTest {
    private TennisScore score;
    @BeforeEach
    void setUp() {
        score =  new TennisScore();
    }

    @AfterEach
    void tearDown() {
        score = null;
    }

    @Test
    void testResultsScore() {
        int[] one = {1, 4, 7, 2, 4};
        int[] two = {3, 4, 2, 4, 4};
        int[] result = {1,2};
        assertArrayEquals(result,score.getPlayerResults(one,two));
    }
}