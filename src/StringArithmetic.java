public class StringArithmetic {
    public String operate(String firstNumber, String secondNumber, String operation){
        if (firstNumber == null || secondNumber == null){
            throw new IllegalArgumentException("Operands should be digits only");
        }
        int firstNum = Integer.parseInt(firstNumber);
        int secondNum = Integer.parseInt(secondNumber);
        int result = 0;
        switch (operation) {
            case "+" -> result = firstNum + secondNum;
            case "-" -> result = firstNum - secondNum;
            case "/" -> result = firstNum / secondNum;
            case "*" -> result = firstNum * secondNum;
            default -> throw new IllegalArgumentException("invalid Operator, Operator must be +,-,* or /");
        }
        return String.valueOf(result);
    }
}
