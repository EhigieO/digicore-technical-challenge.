public class TennisScore {
    public int[] getPlayerResults(int[] playerOneScore, int[] playerTwoScore) {
        int[] result = new int[2];
        if(playerOneScore.length != playerTwoScore.length){
            throw new IllegalArgumentException("number of scores must be equal for both players");
        }
        for (int i = 0; i < playerOneScore.length; i++){
            if (playerOneScore[i] > playerTwoScore[i]){
                result[0] ++;
            } else if (playerOneScore[i] < playerTwoScore[i]){
                result[1] ++;
            }
        }
        return result;
    }
}
